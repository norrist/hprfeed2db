import json
import re
import requests

from peewee import IntegrityError

import data_models

hpr_bot_url = "https://botsin.space/api/v1/accounts/280672/statuses"


def epidode_match(content):
    m = re.findall(r"New Episode: hpr(\d+) :: ", content)
    # print(m)
    if m:
        return m[0]
    return


def get_thread_comments(post_id):
    thread_data = requests.get(
        f"https://botsin.space/api/v1/statuses/{post_id}/context"
    ).json()
    decendendant_list = []
    for decendendant in thread_data["descendants"]:
        decendendant_list.append(decendendant)
    return decendendant_list


def load_comments_from_bot():
    epidode_mastodon_comments = {}
    for status in requests.get(hpr_bot_url).json():
        post_id = epidode_match(status["content"])
        if post_id:
            epidode_mastodon_comments[post_id] = get_thread_comments(status["id"])
    return epidode_mastodon_comments


def load_from_file():
    """Alternate method of loading data for development"""
    with open("comments.json", "r") as f:
        epidode_mastodon_comments = json.load(f)
    return epidode_mastodon_comments


def update_db(epidode_mastodon_comments):
    for show_id in epidode_mastodon_comments:
        for comment in epidode_mastodon_comments.get(show_id):
            pubdate = comment["created_at"]
            author_account = comment["account"]["acct"]
            author_display_name = comment["account"]["display_name"]
            author_avatar_link = comment["account"]["avatar"]
            content = comment["content"]
            comment_url = comment["url"]
            episode_id = show_id
            try:
                data_models.MastodonComment.create(
                    pubdate=pubdate,
                    author_account=author_account,
                    author_display_name=author_display_name,
                    author_avatar_link=author_avatar_link,
                    content=content,
                    comment_url=comment_url,
                    episode_id=f"HPR{episode_id}",
                )
                print(f"Adding Mastodon Comment {episode_id} - {comment_url}")
            except IntegrityError:
                pass


if __name__ == "__main__":
    # epidode_mastodon_comments = load_from_file()
    epidode_mastodon_comments = load_comments_from_bot()

    update_db(epidode_mastodon_comments)
