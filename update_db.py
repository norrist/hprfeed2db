import datetime
import re
from time import mktime

import feedparser
from bs4 import BeautifulSoup
from peewee import IntegrityError

import data_models

# hpr  = feedparser.parse("http://hackerpublicradio.org/hpr_ogg_rss.php")
hpr = feedparser.parse("http://hackerpublicradio.org/hpr_total_ogg_rss.php")  # FULL
# hpr = feedparser.parse("hpr.xml") # Local copy for testing

for entry in hpr.entries:
    # for i in entry:
    #         print(i)
    #         print(entry[i])
    # exit()
    title = entry.title
    episode_id = title.split(":")[0]
    authors = entry.authors
    if entry.itunes_explicit:
        explicit = True
    else:
        explicit = False
    author_name = entry.author_detail.name
    author_email = entry.author_detail.email
    description = entry.description
    link = entry.link
    enclosures = entry.enclosures
    summary = entry.summary
    link = entry.link
    parsed_date = entry.published_parsed
    published = datetime.datetime.fromtimestamp(mktime(parsed_date))
    try:
        data_models.Episode.create(
            explicit=explicit,
            title=title,
            episode_id=episode_id,
            author_name=author_name,
            author_email=author_email,
            link=link,
            description=description,
            pubdate=published,
            summary=summary,
            enclosures=enclosures,
        )
        print(f"Adding {episode_id}")

    except IntegrityError:
        print(f"Skipping {episode_id}")


hpr_comments = feedparser.parse("http://hackerpublicradio.org/comments_rss.php")

for comment in hpr_comments.entries:
    title = comment.title
    link = comment.link
    description = comment.description
    parsed_date = comment.published_parsed
    published = datetime.datetime.fromtimestamp(mktime(parsed_date))
    episode_id = re.search(r"hpr(\d{4})", link).group(0).upper()
    soup = BeautifulSoup(description, "html.parser")
    author_name = soup.strong.a.text

    try:
        data_models.Comment.create(
            title=title,
            episode_id=episode_id,
            author_name=author_name,
            link=link,
            description=description,
            pubdate=published,
        )
        print(f"Adding {title}")
    except IntegrityError:
        print(f"skipping {title}")
