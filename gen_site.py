import glob
import os
import re

import markdown
from jinja2 import Environment, FileSystemLoader

import data_models

episode_data = data_models.Episode.select().order_by(
    data_models.Episode.episode_id.desc()
)

THIS_DIR = os.path.dirname(os.path.abspath(__file__))
j2_env = Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)

html_header = open("templates/html_header").read()
html_footer = open("templates/html_footer").read()

if not os.path.exists("site"):
    os.makedirs("site")
if not os.path.exists("site/episodes"):
    os.makedirs("site/episodes")
if not os.path.exists("site/host"):
    os.makedirs("site/host")


def host_safe_link(host):
    safe_name = "".join([c for c in host if re.match(r"\w", c)])
    return f"/host/{safe_name}.html"


def get_comments(episode_id):
    comments = data_models.Comment.select().where(
        data_models.Comment.episode_id == episode_id
    )
    return comments


def get_mastodon_comments(episode_id):
    mastodon_comments = (
        data_models.MastodonComment.select()
        .where(data_models.MastodonComment.episode_id == episode_id)
        .order_by(data_models.MastodonComment.pubdate)
    )
    return mastodon_comments


def create_episode_pages():
    for episode in episode_data:
        # print(episode.title)
        comments = get_comments(episode.episode_id)
        mastodon_comments = get_mastodon_comments(episode.episode_id)
        enclosure_href = list(eval(episode.enclosures))[0]["href"]
        host_file_name = host_safe_link(episode.author_name)
        outfile = open(f"site/episodes/{episode.episode_id}.html", "w")
        outfile.write(
            j2_env.get_template("templates/episode.html.j2").render(
                {
                    "episode": episode,
                    "comments": comments,
                    "mastodon_comments": mastodon_comments,
                    "html_header": html_header,
                    "html_footer": html_footer,
                    "enclosure_href": enclosure_href,
                    "host_file_name": host_file_name,
                }
            )
        )


def create_index():
    latest_episodes = []
    previous_five_weeks = []
    all_shows = []
    for i, episode in enumerate(episode_data):
        enclosure_href = list(eval(episode.enclosures))[0]["href"]
        data_for_index = {}
        data_for_index["title"] = episode.title
        data_for_index["episode_id"] = episode.episode_id
        data_for_index["author"] = episode.author_name
        data_for_index["pubdate"] = episode.pubdate
        data_for_index["summary"] = (
            re.sub("<[^<]+?>", "", episode.description)[:400] + "..."
        )
        data_for_index["enclosure_href"] = enclosure_href
        data_for_index["host_file_name"] = host_safe_link(episode.author_name)
        all_shows.append(data_for_index)
        if i < 10:
            latest_episodes.append(data_for_index)
        elif i < 35:
            previous_five_weeks.append(data_for_index)

    outfile = open("site/index.html", "w")
    outfile.write(
        j2_env.get_template("templates/index.html.j2").render(
            {
                "latest_episodes": latest_episodes,
                "previous_five_weeks": previous_five_weeks,
                "html_header": html_header,
                "html_footer": html_footer,
            }
        )
    )
    all_outfile = open("site/all.html", "w")
    all_outfile.write(
        j2_env.get_template("templates/all.html.j2").render(
            {
                "all_shows": all_shows,
                "html_header": html_header,
                "html_footer": html_footer,
            }
        )
    )


def create_host_pages():
    hosts = set()
    data_for_hosts_page = []
    for episode in episode_data:
        hosts.add(episode.author_name)
    for host in sorted(hosts):
        # print(host)
        host_file_name = host_safe_link(host)
        host_episodes = []
        host_dates = []
        for episode in episode_data:
            if episode.author_name == host:
                host_episodes.append(episode)
                host_dates.append(episode.pubdate)
        data_for_hosts_page.append(
            {
                "name": host,
                "count": len(host_episodes),
                "page": host_file_name,
                "latest": max(host_dates),
            }
        )
        host_outfile = open(f"site/{host_file_name}", "w")
        host_episode_count = len(host_episodes)
        host_outfile.write(
            j2_env.get_template("templates/host.html.j2").render(
                {
                    "host": host,
                    "host_episodes": host_episodes,
                    "host_episode_count": host_episode_count,
                    "html_header": html_header,
                    "html_footer": html_footer,
                }
            )
        )
        # TODO Build correspondents.html with data_for_hosts_page
        hosts_outfile = open("site/correspondents.html", "w")
        hosts_outfile.write(
            j2_env.get_template("templates/correspondents.html.j2").render(
                {
                    "data_for_hosts_page": data_for_hosts_page,
                    "html_header": html_header,
                    "html_footer": html_footer,
                }
            )
        )


def create_comments_page():
    outfile = open("site/comments.html", "w")
    comments = data_models.Comment.select().order_by(data_models.Comment.pubdate.desc())
    outfile.write(
        j2_env.get_template("templates/comments.html.j2").render(
            {
                "comments": comments,
                "html_header": html_header,
                "html_footer": html_footer,
            }
        )
    )


def create_rss():
    outfile = open("site/rss.xml", "w")
    rss_episodes = []
    for episode in episode_data.limit(10):
        episode_dict = {}
        episode_dict["title"] = episode.title
        episode_dict["link"] = episode.link
        episode_dict["pubdate"] = episode.pubdate
        episode_dict["description"] = episode.description
        enclosure = eval(episode.enclosures)
        # print(enclosure)
        episode_dict["enclosure_href"] = enclosure[0]["href"]
        episode_dict["enclosure_type"] = enclosure[0]["type"]
        episode_dict["enclosure_length"] = enclosure[0]["length"]
        # print(episode.enclosures)
        rss_episodes.append(episode_dict)

    outfile.write(
        j2_env.get_template("templates/rss.xml").render(
            {
                "rss_episodes": rss_episodes,
                "html_header": html_header,
                "html_footer": html_footer,
            }
        )
    )


def build_pages():
    for page_file in glob.glob("pages/*"):
        page_out = page_file.replace("pages/", "site/") + ".html"
        print(f"building page {page_file} {page_out}")
        page_content = open(page_file).read()
        outfile = open(page_out, "w")
        outfile.write(
            j2_env.get_template("templates/page.html.j2").render(
                {
                    "page_content": markdown.markdown(page_content),
                    "html_header": html_header,
                    "html_footer": html_footer,
                }
            )
        )


if __name__ == "__main__":
    create_episode_pages()
    create_index()
    create_host_pages()
    create_comments_page()
    create_rss()
    build_pages()
