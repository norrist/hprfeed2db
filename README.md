# hprfeed2db

## How to Use

- Make sure you have the python dependencies in `requirements.txt`
- Build the Sqlite DB and Static site.
```bash
git clone https://gitlab.com/norrist/hprfeed2db.git
cd hprfeed2db
python3 update_db.py
python3 gen_site.py
```
- Copy the contents of the `site` directory to a web host.

## Gitlab pages
Page output example - https://hpr.norrist.xyz/

## TODO
- ~~Incorporate Comment feed~~
- When posible Mangle comments so links point to static site instead of HPR
- ~~Generate static copy of RSS feed.~~
- Copy content from HPR pages that are not in the RSS feed.
- Generate Tags from Keywords in the show notes.
- Only use the Full Feed on the first run.
