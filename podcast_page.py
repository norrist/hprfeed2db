import logging
import socket
import urllib

from io import BytesIO

import feedparser
import requests
from bs4 import BeautifulSoup

import data_models

logging.basicConfig(level=logging.DEBUG)

episode_data = data_models.Episode.select().order_by(
    data_models.Episode.episode_id.desc()
)


def find_rss(html_text):
    found_feeds = []
    for text_segment in html_text.replace('"', " ").split():
        if text_segment.split(".")[-1] in ("rss", "xml"):
            if text_segment.lower().startswith("http"):
                logging.debug(f"found {text_segment}")
                found_feeds.append(text_segment)
    return set(found_feeds)


def find_links(html_text):
    found_links = []
    soup = BeautifulSoup(html_text, "html.parser")
    # print("The href links are :")
    for link in soup.find_all("a"):
        # if link.get("href") not in feeds_to_ignore:
        logging.debug(f"found {link}")
        found_links.append(link.get("href"))
    return found_links


def feed_is_valid(feed_url):
    logging.debug(f"Testing {feed_url}")
    try:
        resp = requests.get(feed_url, timeout=1)
        d = BytesIO(resp.content)
        d = feedparser.parse(feed_url)
        try:
            d.entries[0].updated_parsed
        except AttributeError:
            d.updated_parsed
        logging.debug("Valid")
        return True
    except (
        urllib.error.URLError,
        AttributeError,
        ConnectionResetError,
        socket.timeout,
        IndexError,
        requests.exceptions.ConnectionError,
        requests.exceptions.ReadTimeout,
    ):
        logging.debug("UrlError")
        return False


def validate_feed_list(feed_list):
    validated_feeds = []
    for feed in feed_list:
        if feed in validated_feeds:
            logging.debug(f"{feed} previosly validatded")
            continue
        if feed_is_valid(feed):
            validated_feeds.append(feed)
    return validated_feeds


def find_feed_in_hpr():
    links = []
    for episode in episode_data:
        print(episode)
        for link in find_rss(episode.description):
            links.append(link)
    validated_feeds = validate_feed_list(set(links))
    return validated_feeds


if __name__ == "__main__":
    feeds = find_feed_in_hpr()
    with open("pages/podcast.md", "w") as f:
        f.write("### Podcasts\n\n")
        for feed in sorted(feeds):
            f.write(f"- {feed}\n")
