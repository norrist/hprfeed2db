from peewee import (
    BooleanField,
    CompositeKey,
    DateTimeField,
    Model,
    OperationalError,
    SqliteDatabase,
    TextField,
)

data_file_name = "hpr.sqlite"
db = SqliteDatabase(data_file_name)


class BaseModel(Model):
    class Meta:
        database = db


class Episode(BaseModel):
    explicit = BooleanField(default=True)
    title = TextField(unique=True)
    episode_id = TextField(unique=True)
    author_name = TextField(default="")
    author_email = TextField(default="")
    link = TextField(default="")
    description = TextField(default="")
    summary = TextField(default="")
    link = TextField(default="")
    enclosures = TextField(default="")
    pubdate = DateTimeField(default="")


class Comment(BaseModel):
    title = TextField()
    episode_id = TextField()
    author_name = TextField()
    link = TextField()
    description = TextField()
    pubdate = DateTimeField()

    class Meta:
        primary_key = CompositeKey("episode_id", "title", "author_name")


class MastodonComment(BaseModel):
    comment_url = TextField()
    episode_id = TextField()
    author_display_name = TextField()
    author_account = TextField()
    author_avatar_link = TextField()
    content = TextField()
    pubdate = DateTimeField()

    class Meta:
        primary_key = CompositeKey(
            "episode_id",
            "comment_url",
        )


db.connect()
try:
    db.create_tables([Episode, Comment, MastodonComment])
except OperationalError:
    pass
